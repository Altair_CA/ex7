import { Component, OnInit } from '@angular/core';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';
import {UsernameValidators} from '../Validators/usernameValdators';

@Component({
  selector: 'app-form2',
  templateUrl: './form2.component.html',
  styleUrls: ['./form2.component.css']
})
export class Form2Component implements OnInit {
  form: FormGroup;

  constructor(fb: FormBuilder) {
      this.form = fb.group({
          username:['',Validators.compose([
            Validators.required,
            UsernameValidators.cannotContainSpace
            ]),
            Validators.composeAsync([
              UsernameValidators.shouldBeUnique
            ])],
          password:['',Validators.required]
      });
   }

  signup(){
    this.form.controls['username'].setErrors({
        invalidLogin: true
    });
    
    console.log(this.form.controls['username']);
    console.log(this.form.value);
  }

  ngOnInit() {
  }

}
