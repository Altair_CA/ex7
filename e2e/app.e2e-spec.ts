import { Ex7Page } from './app.po';

describe('ex7 App', () => {
  let page: Ex7Page;

  beforeEach(() => {
    page = new Ex7Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
